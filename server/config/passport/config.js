const LocalStrategy = require('passport-local').Strategy;
const userModel = require('../../../database/models/userModel');

module.exports = (passport) => {
    passport.use(new LocalStrategy(
        {usernameField: 'email'},
        function (email, password, done) {
            userModel.findOne({email: email}, (err, user) => {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {message: 'Incorrect email.'});
                }
                user.comparePassword(password, (err, isMatch) => {
                    if (err) {
                        return done(err);
                    }
                    if (!isMatch) {
                        return done(null, false, {message: 'Incorrect password.'});
                    }
                    return done(null, user);
                });
            });
        }
    ));

    passport.serializeUser((user, done) => {
        done(null, user._id);
    });

    passport.deserializeUser((id, done) => {
        userModel.findById(id, '-password', (err, user) => {
            done(err, user); //req.user._id
        });
    });
};
