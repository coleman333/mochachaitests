const passport = require('passport');
const path = require('path');
const multer = require('multer');
let bcrypt = require('bcrypt');
let SALT_WORK_FACTOR = 10;
let userModel = require('../../database/models/userModel');
const _ = require('lodash');

module.exports.upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            const absolutePath = path.join(__dirname, '../../images/');
            cb(null, absolutePath);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    })
});

module.exports.registration = function (req, res, next) {
    // console.dir(JSON.parse(req.body.user));
    //  req.body.user=JSON.parse(req.body);
    console.dir(req.body);
    // console.dir(req.file);
    let user = req.body;

    try {
        user.avatar = `/images/${req.file.filename}`; //'images'+req.file.filename
    } catch (ex) {
    }
    userModel.create(user, function (err, user) {
        if (err) {
            console.error(err);
            return next(err);
        }
        req.logIn(user, function (err) {
            if (err) {
                console.error(err);
                return next(err);
            }
            res.json(_.omit(user.toJSON(), 'password'));
        });
        // login2(req.body.user);
        // res.json(_.omit(user.toJSON(), 'password'));
    });

};

function encryptPassword(password, callbackEncryptPassword) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return callbackEncryptPassword(err);

        // hash the password along with our new salt
        bcrypt.hash(password, salt, function (err, hash) {
            if (err) return callbackEncryptPassword(err);
            // override the cleartext password with the hashed one
            callbackEncryptPassword(null, hash);
        });
    });
}


module.exports.login = function (req, res, next) {
    // encryptPassword(user.password, function (err, encryptedPassword) {
    //     if (err) {
    //         console.error(err);
    //         return next(err);
    //     }
    //     user.password = encryptedPassword;


    passport.authenticate('local', function done(err, user, info) {
        // let userModel ={};
        // userModel.find({category: req.params.email},function (err,user) {
        //
        // });
        if (err) {
            console.error(err);
            return next(err);
        }
        if (user) {
            req.logIn(user, function (err) {
                if (err) {
                    console.error(err);
                    return next(err);
                }
                res.json({
                    _id: user._id.toString(),
                    firstName: user.firstName,
                    avatar: user.avatar
                    // message: 'You have access!'
                });
            });
        } else {
            res.statusCode = 403;
            res.json({
                message: info.message
            });
        }
    })(req, res, next);
    // })
};

module.exports.isEmail = function (req, res, next) {
    // let isEmailResult = undefined;
    let email = req.body.email;
    console.log('server', email);

    userModel.find({email: email}, function (err, isEmailResult) {
        if (err) {
            console.error(err);
            return next(err);
        }
        return res.json({
            isEmailResult: !!isEmailResult.length
        });

        // if (isEmailResult) {
        //     // console.log("server",isEmailResult);
        //     res.json({
        //         isEmailResult: true
        //     })
        // }
        // else {
        //     // console.log("serverFalse",isEmailResult);
        //     res.json({
        //         isEmailResult: false
        //     })
        // }
    })

};

module.exports.isUser = function (req, res, next) {
    if (req.isAuthenticated()) {
        // console.log(req.user);
        res.json({
            name: req.user.name,
            avatar: req.user.avatar
        });
    }
    else {
        res.statusCode = 403;
        res.end()
    }
};

module.exports.logout = function (req, res, next) {
    req.logOut();
    res.end();
};

module.exports.fetch = function (req, res, next) {
    userModel.find({}, (err, users)=> {
        if (err) {
            console.error(err);
            return next(err);
        }
        setTimeout(()=> res.json({users: users}), 2000);
        // setTimeout(function (){res.json({users: users})}, 2000)
    })
};






