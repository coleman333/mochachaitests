const { expect } = require('chai');
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store'

import MainPage from './index';

const mockStore = configureStore();
const initialState = {};
let store;

describe('Main page test case', () => {
    beforeEach(() => {
        store = mockStore(initialState)
    });

    it('MainPage should contain content', () => {
        store = mockStore({'test': '123'});
        const wrapper = mount(<MainPage children={'He2llo'} store={store}/>);
        expect(wrapper.find('.mainPage').text()).to.include('He2llo');
        expect(wrapper.find('.mainPage').text()).to.include('123');
        console.log(wrapper.text());

        expect({a: 1}).to.not.have.property('b');
    });
    it('MainPage should contain content', () => {

        expect({a: 1}).to.not.have.property('b');
    });
});