import "../../assets/styles/main.scss";
import './styles.scss';
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import {browserHistory} from 'react-router';


class MainPage extends Component {
	static propTypes = {};

	state = {
		arrNewObject:[
			{
				id: 1,
				partnerName: 'partner1',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 2,
				partnerName: 'partner2',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 3,
				partnerName: 'partner3',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'приостановлена'
			},
			{
				id: 4,
				partnerName: 'partner4',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 5,
				partnerName: 'partner5',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'приостановлена'
			},
			{
				id: 6,
				partnerName: 'partner6',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 7,
				partnerName: 'partner7',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 8,
				partnerName: 'partner8',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'приостановлена'
			},
			{
				id: 6,
				partnerName: 'partner6',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 7,
				partnerName: 'partner7',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'новая'
			},
			{
				id: 8,
				partnerName: 'partner8',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				status: 'приостановлена'
			}
		]
	};

	constructor(props) {
		super(props);
	}

	render() {

		const {children, test} = this.props;

		return <div className="mainPage">
			MAIN page
			{children}
			{test}
			{this.state.arrNewObject.map((item)=>{
			return <div>
				<span>{item.partnerName}</span>
				<span>{item.date}</span>
				<span>{item.bonusRequest}</span>
			</div>
			})}
		</div>
	}

}

const mapStateToProps = (state) => {
	return {
		test: state.test
	}
};

const mapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
