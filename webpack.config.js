const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const env = process.env.NODE_ENV;

const hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';
const assetsPath = path.join(__dirname, 'public', 'assets');
const publicPath = '/assets/';

const plugins = [
	// extract inline css from modules into separate files
	new ExtractTextPlugin('styles/main.css'),
	new webpack.EnvironmentPlugin({
		ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
		DEBUG: false
	}),
];

if (env === 'production') {
	// Order the modules and chunks by occurrence.
	// This saves space, because often referenced modules
	// and chunks get smaller ids.
	plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
	plugins.push(new webpack.optimize.UglifyJsPlugin({
		beautify: false,
		comments: false,
		compress: {
			dead_code: true,
			booleans: true,
			loops: true,
			unused: true,
			warnings: false,
			drop_console: true
		}
	}));
} else {
	plugins.push(new webpack.HotModuleReplacementPlugin());
}

let commonLoaders = [
	{
		test: /\.js$|\.jsx$/,
		loaders: ['babel-loader'],
		include: path.join(__dirname, 'front'),
		exclude: /node_modules/
	},
	// {test: /\.json$/, loader: 'json-loader'},
	{test: /\.(eot|woff|woff2|ttf|svg|png|gif)([\?]?.*)$/, loader: 'url-loader'},
	{test: /\.jpg$/, loader: 'file-loader'},
	{
		test: /\.scss$/,
		loader: ExtractTextPlugin.extract({
			fallback: 'style-loader',
			use: [
				{
					loader: "css-loader"
				},
				{
					loader: "sass-loader",
					options: {
						includePaths: [encodeURIComponent(path.resolve(__dirname, 'front', 'assets', 'styles'))]
					}
				}
			]
		})
	}
];

module.exports = [
	{
		mode: env,
		// The configuration for the client
		name: 'browser',
		// A SourceMap is emitted.*/
		devtool: env === 'production' ? 'source-map' : 'eval',
		context: path.join(__dirname, 'front'),
		entry: {
			front: env === 'production' ? ['babel-polyfill', './client'] : ['babel-polyfill', hotMiddlewareScript, './client']
		},
		output: {
			// The output directory as absolute path
			path: assetsPath,
			// The filename of the entry chunk as relative path inside the output.path directory
			filename: '[name].js',
			// The output path from the view of the Javascript
			publicPath: publicPath

		},

		module: {
			rules: commonLoaders
		},
		resolve: {
			extensions: ['.js', '.jsx', '.scss'],
			modules: [
				'front', 'node_modules'
			],
			alias: {
				app: path.resolve(__dirname, 'front/')
			}
		},
		plugins: plugins.concat([new webpack.DefinePlugin({
			__SERVER__: false
		})])
	}, {
		mode: env,
		// The configuration for the server-side rendering
		name: 'server-side rendering',
		context: path.join(__dirname, 'front'),
		entry: {
			front: './server'
		},
		target: 'node',
		output: {
			// The output directory as absolute path
			path: assetsPath,
			// The filename of the entry chunk as relative path inside the output.path directory
			filename: '[name].server.js',
			// The output path from the view of the Javascript
			publicPath: publicPath,
			libraryTarget: 'commonjs2'
		},
		module: {
			rules: commonLoaders
		},
		resolve: {
			extensions: ['.js', '.jsx', '.scss'],
			modules: [
				'front', 'node_modules'
			],
			alias: {
				app: path.resolve(__dirname, 'front/')
			}
		},
		plugins: plugins.concat([new webpack.DefinePlugin({
			__SERVER__: true
		})])
	}
];
