require('babel-register')();
const jsdom = require('jsdom');
const enzyme = require('enzyme');
const adapter = require('enzyme-adapter-react-16');


enzyme.configure({ adapter: new adapter() });

const { JSDOM } = jsdom;
const dom = new JSDOM('<!doctype html><html><body></body></html>', {url: 'http://localhost/'});
const { window } = dom;

global.window = window;
global.document = window.document;
global.navigator = {
    userAgent: 'node.js',
};

copyProps(window, global);

function copyProps(src, target) {
    const props = Object.getOwnPropertyNames(src)
        .filter(prop => typeof target[prop] === 'undefined')
        .map(prop => Object.getOwnPropertyDescriptor(src, prop));
    Object.defineProperties(target, props);
}